/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ania;

/**
 *
 * @author Radziu
 */
public class Etykieta {
    
    private String pole1;
    private String pole2;
    private String pole3;
    private String sala;
    
    public Etykieta() {}
    
    public Etykieta(String pole1, String pole2, String pole3, String sala) {
        this.pole1 = pole1;
        this.pole2 = pole2;
        this.pole3 = pole3;
        this.sala = sala;
    }
    
    @Override
    public String toString() {
        return pole1 + ";" + pole2 + ";" + pole3 + ";" + sala + ";";
    }
    
}
